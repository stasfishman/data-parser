import gzip
import re
import os

START_PATTERN = '-----BEGIN HOSTINFO-----'
END_PATTERN = '-----END HOSTINFO-----'
NUMBER_OF_CUSTOMERS = 1295




def parse_file_from_gz_to_txt(gz_file_path):
    with gzip.open(gz_file_path, 'r') as fin:
        with open('/home/stas/PycharmProjects/Parser_for_Itay/parsed_file.txt', 'w') as p:
            for line in fin:
                line = str(line.decode('utf-8'))
                # print(line)
                p.write(line)
    fin.close()
    p.close()


def append_to_parsed_file(id, count):
    global num_of_lines
    regex = ",[Sun|Mon|Tue|Wed|Thu|Fri|Sat].*?[MyTZ_4|CEST|WIB].*?,"
    with open('/home/stas/PycharmProjects/Parser_for_Itay/parsed_file.txt', 'r') as file:
        match = False
        newfile = None
        version = ''
        profiles = ''
        for line in file:
            if line.startswith('Version'):
                version = line
                version = version.partition(': ')[-1].rstrip()
                break
        for line in file:
            if line.startswith('| profiles'):
                profiles = line
                profiles = profiles.split("|")[-2]
                break
        for line in file:
            if re.match(START_PATTERN, line):
                match = True
                newfile = open(f'/home/stas/PycharmProjects/Parser_for_Itay/{count}.txt', 'a')
                continue
            elif re.match(END_PATTERN, line):
                match = False
                newfile.close()
                continue
            elif match:
                if 'ebanner_http' in line:
                    # print(line)
                    line = re.sub(regex, '*', line)
                    # print(line)
                    customer_id = '{}* '.format(id)
                    customer_id_version_profiles = customer_id + version + '* ' + profiles + '*'
                    # print(customer_id_version)
                    line = line.replace(',', '*', 2)
                    newline = customer_id_version_profiles + line
                    if version.startswith('8.'):
                        newline = newline.replace('ebanner_http,','ebanner_http*')
                    # print(newline)
                    newfile.write(newline)
                    num_of_lines += 1
                # line = re.sub(regex, '; ', line)
                # customer_id = '{}; '.format(id)
                # customer_id_version_profiles = customer_id + version + '; ' + profiles + ';'
                # # print(customer_id_version)
                # line = line.replace(',', ';', 3)
                # newline = customer_id_version_profiles + line
                # newfile.write(newline)
                # # print(newline)
                # num_of_lines += 1


def get_list_of_gzip_files_per_customer_id(id):
    for root, dirs, files in os.walk("/home/stas/Desktop/files_for_parsing/{}".format(id)):
        for file in files:
            if file.endswith(".gz"):
                 arr.append(os.path.join(root, file))
    return arr

arr = []
list_of_customers = []
total_list_of_customers = os.listdir("/home/stas/Desktop/files_for_parsing/")
count = 1
while len(total_list_of_customers) > 0:
    num_of_lines = 0
    print('total list of customers: {}'.format(total_list_of_customers))
    for i in range(NUMBER_OF_CUSTOMERS):
        try:
            list_of_customers.append(total_list_of_customers[-1])
            total_list_of_customers.pop()
        except Exception:
            pass
    print('list of customers: {}'.format(list_of_customers))
    print('count = {}'.format(count))
    for l in list_of_customers:
        res = get_list_of_gzip_files_per_customer_id(l)
        # print(res)
        try:
            for val in res:
                parse_file_from_gz_to_txt(val)
                append_to_parsed_file(l, count)
        except Exception as e:
            print(e)
        arr = []
    list_of_customers = []
    print(f"File {count}.txt contains {num_of_lines} lines.")
    count += 1





